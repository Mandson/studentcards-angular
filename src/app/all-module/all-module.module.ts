import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import { MatSliderModule } from '@angular/material/slider';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import {MatMenuModule} from '@angular/material/menu'; 
import {MatIconModule} from '@angular/material/icon'; 
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';  
const Materials=[
  MatButtonModule,
MatToolbarModule,
MatGridListModule,
MatCardModule,
MatSliderModule,
MatFormFieldModule,
MatMenuModule,
MatIconModule,
MatTableModule,
MatDatepickerModule
];

@NgModule({
  declarations: [],
  imports: [
    Materials
  ],
  
 exports:[Materials]
})
export class AllModuleModule { }
