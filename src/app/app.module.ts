import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AllModuleModule} from './all-module/all-module.module';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { LogintopComponent } from './logintop/logintop.component';
import { LogincentreComponent } from './logincentre/logincentre.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { EnregistrementComponent } from './enregistrement/enregistrement.component';
import { ListeComponent } from './liste/liste.component';
import { ModifierComponent } from './modifier/modifier.component';
import { DetailComponent } from './detail/detail.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginpageComponent,
    LogintopComponent,
    LogincentreComponent,
    SidenavComponent,
    EnregistrementComponent,
    ListeComponent,
    ModifierComponent,
    DetailComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AllModuleModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
