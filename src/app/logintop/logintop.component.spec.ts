import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogintopComponent } from './logintop.component';

describe('LogintopComponent', () => {
  let component: LogintopComponent;
  let fixture: ComponentFixture<LogintopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogintopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogintopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
