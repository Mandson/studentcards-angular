import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogincentreComponent } from './logincentre.component';

describe('LogincentreComponent', () => {
  let component: LogincentreComponent;
  let fixture: ComponentFixture<LogincentreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogincentreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogincentreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
